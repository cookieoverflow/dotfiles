# === Ruby ===
if command -v chruby-exec &> /dev/null; then
  source /usr/local/opt/chruby/share/chruby/chruby.sh
  # source /usr/local/opt/chruby/share/chruby/auto.sh
fi

alias ruby-server="ruby -r webrick -e \"s = WEBrick::HTTPServer.new(:Port => 8000, :DocumentRoot => Dir.pwd); trap('INT') { s.shutdown }; s.start\""

alias be='bundle exec'

# === Git ===

parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

function parse_git_dirty {
  [ -d .git ] && [[ $(git diff --stat) != '' ]] && echo "*"
}

USER_INFO="\u@\h "
CURRENT_DIR="\[\e[32m\]\w "
GIT_INFO="\[\e[36m\]\$(parse_git_branch)\[\e[00m\]"
GIT_DIRTY="\[\e[91m\]\$(parse_git_dirty)\[\e[00m\]"
export PS1="$CURRENT_DIR$GIT_INFO$GIT_DIRTY $> "

alias g='git'
alias gl='git log --pretty=format:"%C(yellow)%h %Cred%ad %Cblue%an%Cgreen%d %Creset%s" --date=short -n 20'
alias gap='git add -p'
alias gfm='git checkout master; git fetch origin master; git reset --hard origin/master'
alias gri='git rebase -i --autosquash'
alias gcleanbranches='git remote prune origin;git branch --merged master | grep -v "master" | xargs git branch -d;'

# Git fetch checkout remote
gfcor() {
  git fetch origin "$1"
  git checkout -b "$1" origin/"$1"
}

gfb() {
  git checkout "$1"
  git fetch origin "$1"
  git reset --hard origin/"$1"
}

# === Postgres ===
PG_APP="/Applications/Postgres.app/Contents/Versions/latest/bin" # mac only
[ -f $PG_APP ] && export PATH="$PG_APP:$PATH"

export PGHOST=localhost
export PGPORT=5432

# === Work ===
[[ -s ~/dotfiles/work.sh ]] && source ~/dotfiles/work.sh

# === Aliases ===
alias ytmp3='youtube-dl -x --audio-format mp3' # download vid as mp3
alias kata='cd ~/Projects/learn/ruby/katas'
alias notes='vim ~/Documents/notes'

# === Other ===
[[ -s ~/dotfiles/sensitive.sh ]] && source ~/dotfiles/sensitive.sh

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
