#!/usr/bin/env bash

# Create backup of existing dotfile and symlink to the one in ~/.dotfiles
function link_file() {
  FILE=$1
  if [ -e ~/$FILE ]; then
    mv ~/$FILE "~/$FILE.bak"
  fi
  ln -s ~/dotfiles/$FILE ~/$FILE
}

# install ruby
if command -v brew &> /dev/null; then
  brew install chruby
  brew install ruby-install
fi

# configure git
link_file .gitconfig
link_file .gitignore_global

# setup vim with plugins
link_file .vimrc
mkdir -p ~/.vim/undodir

# install vim-plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# install all plugins listed in .vimrc
vim -c 'PlugInstall' -c 'qa!'

link_file .inputrc
link_file .bashrc

echo ">>> Install completed successfully"
echo "Edit ~/.gitconfig to set you git username and email"

