#!/usr/bin/env bash

function unlink_file() {
  FILE=~/$1
  BACKUP=$FILE.bak
  rm $FILE
  if [ -e $BACKUP ]; then
    mv $BACKUP $FILE
  fi
}

# Reset git
unlink_file .gitconfig
unlink_file .gitignore_global

# Reset vim
rm -rf ~/.vim/bundle
unlink_file .vimrc

unlink_file .inputrc
unlink_file .bashrc

echo ">>> Uninstall completed successfully, please restart all your sessions"
