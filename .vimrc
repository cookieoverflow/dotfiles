syntax on

set noerrorbells
set tabstop=2 softtabstop=2
set shiftwidth=2
set expandtab
set smartindent
set autoindent
set nu
set relativenumber
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set ruler

set colorcolumn=80
highlight ColorColumn ctermbg=0 guibg=lightgrey

call plug#begin('~/.vim/plugged')

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'morhetz/gruvbox'
Plug 'jremmen/vim-ripgrep'
Plug 'tpope/vim-fugitive'
Plug 'vim-utils/vim-man'
Plug 'mbbill/undotree'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'dense-analysis/ale'
Plug 'vim-airline/vim-airline'

" Ruby/Rails pugins
Plug 'tpope/vim-rails'
Plug 'thoughtbot/vim-rspec'
Plug 'tpope/vim-endwise' " Adds 'end' to keywords like if,class,def etc.
Plug 'slim-template/vim-slim'

" these two only work in neovim
"Plug 'theprimeagen/vim-apm'
"Plug 'theprimeagen/vim-be-good', { 'do': './install.sh' }

call plug#end()

colorscheme gruvbox
set background=dark

" Load indent and plugin file for detected filetype
filetype plugin indent on

let mapleader=" "
let localmapleader = ","              " Specific to filetypes

let g:netrw_browse_split=2
let g:netwrw_banner=1 "disable this once familiarised with commands
let g:netrw_winsize=25
let backspace=2 " make backspace work like most other programs

" let ripgrep detect git root and use .gitignore
if executable('rg')
  let g:rg_derive_root='true'
endif

"Remember last line position
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.8 } }
let $FZF_DEFAULTS_OPS='--reverse'

"nnoremap <leader>h :wincmd h<CR>
"nnoremap <leader>j :wincmd j<CR>
"nnoremap <leader>k :wincmd k<CR>
"nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>u :UndotreeShow<CR>
nnoremap <leader>pv :wincmd v<bar> :Ex <bar> :vertical resize 30<CR>
nnoremap <leader>ps :Rg<SPACE>
nnoremap <leader>f :FZF<CR>
nnoremap <silent> <leader>= :vertical resize +5<CR>
nnoremap <silent> <leader>+ :vertical resize +5<CR>
nnoremap <silent> <leader>- :vertical resize -5<CR>
nnoremap <leader>ev :tabe $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

" move vertically by visual line
nnoremap j gj
nnoremap k gk
nnoremap <leader>j ddp " move line down
nnoremap <leader>k ddkP " move line up
"uppercase word in insert mode
inoremap <c-u> <esc>viwUi
map <Leader>t :call RunCurrentSpecFile()<CR>
map <Leader>s :call RunNearestSpec()<CR>

"open buffer list with buffer command ready to take buffer number.
nnoremap <leader>b :ls<CR>:buffer<space>

" display » for tabs and ˽ for trailing whitespace
set list listchars=tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:˽
set backspace=indent,eol,start
